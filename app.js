const express = require("express");
const uploadRoutes = require("./api/routes/uploads");
const userRoutes = require("./api/routes/user");
const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cors = require("cors");
const Upload = require("./api/models/upload");
const path = require("path");
const checkAuth = require("./api/middleware/auth-check");

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

mongoose.connect(
  "mongodb://TM:123456@ds227740.mlab.com:27740/shop-rest-api",
  { useNewUrlParser: true }
);

app.use(cors());
app.use(morgan("dev"));
//Authenticated download of the files
app.use("/files", checkAuth, (req, res, next) => {
  const userId = req.userData.userId;
  
  Upload.find()
    .select("file userId ")
    .exec()
    .then(docs => {
      reqFile=req.originalUrl.slice(1)
      let doc =docs.filter(el=>el.userId===userId).find(em=>em.file===reqFile)
    
      if (doc) {
        res.sendFile(path.join(__dirname,doc.file));
      } 
       else {
       res.status(409).send({ message: "no access to this file" });
      }
    })
    .catch(err => {
      res.status(500).send({ message: err });
    });
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});
// Our routes added here

app.use("/uploads", uploadRoutes);
app.use("/user", userRoutes);

app.use((req, res, next) => {
  const error = new Error("its all empty here");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: { message: error.message }
  });
});
module.exports = app;
