# Backend

Backend is built with Express.js, Mongodb and other utility tools

  - The idea was to serve the  React app and persist all the requests to the api end point.
  -It is a rather basic crud app
  


### Tech

The following techs are used

* [Node js]
* [express]-framework for building Node application
* [mongodb] - Database
* [JWT]- web token based Authentication




### Installation

Install the dependencies and devDependencies and start the server.

```sh
clone the repository

$ cd [directory]
$  npm install
$   npm start
```












