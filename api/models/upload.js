const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const uploadSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  userId: { type: String, required: true },
  name: { type: String, required: true },
  file: { type: String, required: true },
  size: { type: String, required: true }
});

module.exports= mongoose.model('Upload', uploadSchema)