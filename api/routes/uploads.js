const express = require('express')
const router = express.Router()
const Upload= require('../models/upload')
const mongoose= require('mongoose')
const checkAuth = require("../middleware/auth-check");
const multer = require("multer");
const path = require("path");
const fs = require('fs')




const sizeCalc = (bytes, decimalPoint) => {
  if (bytes === 0) return "0 Bytes";
  let k = 1000,
    dm = decimalPoint || 2,
    sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
    i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
};






// using multer to store files in a static folder

const storage = multer.diskStorage({
  destination: (req, file, cb) =>{
    cb(null, "./files/");
  },
  filename: (req, file, cb) =>{
    cb(null, new Date().toISOString()+ path.extname(file.originalname));
  }
});

// passes all arg to multer storage
const upload = multer({
  storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  }

});


// root get route
router.get('/',checkAuth, (req, res, next) => {
  const userId= req.userData.userId

  Upload.find()
    .select("name userId _id file size")

    .exec()
    .then(docs => {
      if (docs.length > 0) {
        let response = docs.filter(doc => {
            return doc.userId===userId && doc;
          })
          if(response.length<1){
             res
               .status(200)
               .json({ message: "no image is uploded" });
          }
      else {  res.status(200).json({ response });}
      } else {
        let response = { message: "no image is uploded" };
        res.status(200).json({ response });
      }
    })
    .catch(err => {
      res.status(500).send({ message: err });
    });


})

// root post route
router.post('/', checkAuth, upload.single('file') ,(req, res, next) => {

  const upload = new Upload({
    _id: new mongoose.Types.ObjectId(),
    userId: req.userData.userId,
    name: req.file.originalname,
    file: req.file.path,
    size: sizeCalc(req.file.size)
  });
  upload
    .save()
    .then(result => {
      res
        .status(201)
        .json({
          message: "new file upload is uploaded",
          newUpload: {
            name: result.name,
            id: result._id,
            userId:result.userId,
            file: result.file,
            size:result.size
          }
        });

    })
    .catch(err => {
      res.status(500).send({ message: err });
    });


});

// root /:Id route
router.get("/:Id",checkAuth, (req, res, next) => {
  const id = req.params.Id
  Upload.findById(id)
    .select("name _id file")
    .exec()
    .then(doc => {
      if (doc) {
        res
          .status(200)
          .json({
            upload: doc,

          });
      } else {
        res.status(500).send({ message: "no file found for this id" });
      }
    })
    .catch(err => {
      res.status(500).send({ message: err });
    });

});



//  delete route
router.delete("/:Id",checkAuth, async(req, res, next) => {
  const userId = req.userData.userId;
  const id = req.params.Id;
  const file = await Upload.findById(id).select("name _id userId file size").exec();

  Upload.remove({ _id: id })
    .exec()
    .then(() => {

      fs.unlink(file.file, err => {
       if(err) console.log(err)
     res.status(200).json({ message: file.name + " is deleted" });

      });

    })
    .catch(err => {
      res.status(500).send({ message: err });
    });

});


module.exports= router;
